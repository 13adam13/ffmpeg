﻿$Refresh = 0
$Final = 0
$FFMpeg = "ffmpeg.exe"

If ($Refresh){
    echo "Path,Bookend" > mylist.csv
    $files = @(Get-ChildItem "C:\Users\awilson\R Fox Consulting\Friend For Life Cancer Support Network - 2020 QWF\2020 EWF Videos\finalized\*.mp4")
    foreach ($file in $files) {
        echo "$file,TRUE" >> mylist.csv
    }
}

Import-Csv mylist.csv| ForEach-Object {
    if($file = Get-Item $_.Path) {
    $bookend = $_.Bookend
    echo "Working with $file"
    $StartFile = "StartFile.mp4"
    $EndFile = "EndFile.mp4"
    $MergeFile = "MergeFile.mp4"
    $FinalFile = $file.Name.Replace(".mp4","-800.mp4")

    echo "Creating and scaling $EndFile"
    &$FFMpeg -hide_banner -loglevel warning -y -i $file -video_track_timescale 600 -vf scale=800:-2:flags=bicubic $EndFile

    if ($bookend = 1){
        echo "Creating mylist.txt"
        echo "file '$StartFile'" > mylist.txt
        echo "file '$EndFile'" >> mylist.txt
        echo "file '$StartFile'" >> mylist.txt
        (Get-Content 'mylist.txt') | Set-Content 'mylist.txt' -Encoding Ascii
        echo "Creating $StartFile"
        &$FFMpeg -hide_banner -loglevel warning -y -i $EndFile -vf trim=0:3,geq=0:128:128 -af atrim=0:3,volume=0 -video_track_timescale 600 $StartFile
        echo "Merging into $MergeFile"
        &$FFMpeg -hide_banner -loglevel warning -y -f concat -i mylist.txt -c copy $MergeFile
        echo "fading into $FinalFile"
        &$FFMpeg -hide_banner -loglevel warning -y -i $MergeFile -af afade=t=in:st=3:d=4 $FinalFile
    } else {
        echo "fading into $FinalFile"
        &$FFMpeg -hide_banner -loglevel warning -y -i $EndFile -af afade=t=in:st=3:d=4 $FinalFile
    }
    If ($Final -eq 1) {
            echo "Copying to Sharepoint"
            Copy-Item $FinalFile -Destination "C:\Users\awilson\R Fox Consulting\Friend For Life Cancer Support Network - 2020 QWF\2020 EWF Videos\adam"
        }
    }
}